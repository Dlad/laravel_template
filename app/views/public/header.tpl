<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="zh-CN">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no" />
	<meta charset="UTF-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="icon" type="image/png" href="{$smarty.const.__IMAGE_URL}/favicon.png" />
	<link rel="apple-touch-icon" href="{$smarty.const.__IMAGE_URL}/touch-icon.png" />

    <script src="{$smarty.const.__JS_URL}/jquery-2.1.3.min.js" ></script>
    <script src="{$smarty.const.__JS_URL}/bootstrap.min.js" defer async="true" ></script>

	<!--[if IE]>
    <script src="{$smarty.const.__JS_URL}/html5.js"></script>
	<![endif]-->

    <link rel="stylesheet" href="{$smarty.const.__CSS_URL}/bootstrap.min.css" />
    <link rel="stylesheet" href="{$smarty.const.__CSS_URL}/bootstrap-theme.min.css" />
    <link rel="stylesheet" href="{$smarty.const.__CSS_URL}/main.css"/>

    <link href="//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">

    {if $cssQueue}
        {foreach item=cssFile from=$cssQueue}
            <link rel="stylesheet" href="{$smarty.const.__CSS_URL}/{$cssFile}"/>
        {/foreach}
    {/if}
	<title>{$frontTitle}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>

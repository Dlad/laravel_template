<?php

class TestController extends Controller {

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    function index()
    {

        $data = array();
        $data['ss'] = 'Hello, world!';
        return View::make('test', $data);
    }

}